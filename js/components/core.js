if (typeof Object.create !== 'function') {
    Object.create = function(obj) {
        function F() { };
        F.prototype = new obj;
        return F();
    }
}
