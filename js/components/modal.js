(function($, window, document, undefined) {
    var LazyModal = {
        init: function(options, elem) {
            var self = this;
            self.elem = elem;
            self.$elem = $(elem);
            self.isOpen = false;
            self.options = $.extend({}, $.fn.modal.options, options);
            self.widthPercentage = self.$elem.width() / self.$elem.parent().width() * 100;
            self.height = self.$elem.height();
            
            self.closeEvents();
            self.modalStartState();
            
            if (self.options.show) {
                self.animation();
            }
            
            if (self.options.target !== undefined) {
                self.target = $(self.options.target);
                self.openEvent();
            }
        },
        openEvent: function() {
            var self = this;
            self.target.on('click', function() {
                if (!self.isOpen) {
                    self.animation();
                }
            });
        },
        closeEvents: function() {
            var self = this;
           
            self.$elem.find('.close').on('click', function() {
                if (self.isOpen) {
                    self.animation();
                }
            });
            
            if (self.options.closeOnOutsideClick) {
                $('html').on('click', function(e) {
                    if (!self.$elem.is(e.target) && !self.$elem.has(e.target).length && self.isOpen) {
                        self.animation();
                    }
                });
            }
        },
        disableBackground: function(doAppend) {
            var self = this;
            if (self.options.disableBackground) {
                if (doAppend) {
                    $('<div></div>', {
                        class: 'modal-disable-bg'
                    }).appendTo('body').fadeIn(200);
                } else {
                    $('.modal-disable-bg').fadeOut(200, function() {
                        $(this).remove();
                    });
                }
            }
        },
        modalStartState: function() {
            var self = this;
            switch (self.options.animation) {
                case 'resize':
                    self.$elem.css('width', '15%').css('display', 'none').css('opacity', 0);
                    break;
                case 'left':
                    self.$elem.css('left', '-100%').css('display', 'block');
                    break;
                case 'right':
                    self.$elem.css('left', '100%').css('display', 'block');
                    break;
                case 'top':
                    self.$elem.css('top', (-self.height - 20)+'px').css('display', 'block');
                    break;
                case 'bottom':
                    self.$elem.css('top', '100%').css('display', 'block');
                    break;
            }
        },
        animation: function() {
            var self = this;
            self.disableBackground(!self.isOpen);
            
            switch (self.options.animation) {
                case 'slide':
                    self.slideAnimation();
                    break;
                case 'resize':
                    self.resizeAnimation();
                    break;
                case 'left':
                    self.horizontalAnimation('-100%');
                    break;
                case 'right':
                    self.horizontalAnimation('100%');
                    break;
                case 'top':
                    self.verticalAnimation((-self.height - 20)+'px');
                    break;
                case 'bottom':
                    self.verticalAnimation('100%');
                    break;
                default:
                    self.fadeAnimation();
                    break;
            }
            
            self.modalStateCallback();
        },
        fadeAnimation: function() {
            var self = this;
            if (!self.isOpen) {
                self.$elem.fadeIn(self.options.duration, function() {
                    self.options.onOpen();
                    self.switchOpenState();
                });
            } else {
                self.$elem.fadeOut(self.options.duration, function() {
                    self.options.onClose();
                    self.switchOpenState();
                });
            }
        },
        slideAnimation: function() {
            var self = this;
            if (!self.isOpen) {
                self.$elem.slideDown(self.options.duration, function() {
                    self.options.onOpen();
                    self.switchOpenState();
                });
            } else {
                self.$elem.slideUp(self.options.duration, function() {
                    self.options.onClose();
                    self.switchOpenState();
                });
            }
        },
        resizeAnimation: function() {
            var self = this;      
            if (!self.isOpen) {
                self.$elem.animate({ 
                    width: self.widthPercentage+'%',
                    opacity: 1 
                }, {
                    duration: self.options.duration,
                    start: function() {
                        self.$elem.css('display', 'block');
                    },
                    complete: function() {
                        self.options.onOpen();
                        self.switchOpenState();
                    }
                });
            } else {
                self.$elem.animate({
                    width: '15%',
                    opacity: 0
                }, {
                    duration: self.options.duration,
                    complete: function() {
                        self.options.onClose();
                        self.$elem.css('display', 'none');
                        self.switchOpenState();
                    }
                });
            }
        },
        horizontalAnimation: function(pos) {
            var self = this;
            if (!self.isOpen) {
                self.$elem.animate({ left: '0' }, {
                    duration: self.options.duration,
                    complete: function() {
                        self.options.onOpen();
                        self.switchOpenState();
                    }
                });
            } else {
                self.$elem.animate({ left: pos }, {
                    duration: self.options.duration,
                    complete: function() {
                        self.options.onClose();
                        self.switchOpenState();
                    }
                });
            }
        },
        verticalAnimation: function(pos) {
            var self = this;
            if (!self.isOpen) {
                self.$elem.animate({ top: '100px' }, {
                    duration: self.options.duration,
                    complete: function() {
                        self.options.onOpen();
                        self.switchOpenState();
                    }
                });
            } else {
                self.$elem.animate({ top: pos }, {
                    duration: self.options.duration,
                    complete: function() {
                        self.options.onClose();
                        self.switchOpenState();
                    }
                });
            }
        },
        modalStateCallback: function() {
            var self = this;
            if (!self.isOpen) {
                self.options.onOpening();
            } else {
                self.options.onClosing();
            }
        },
        switchOpenState: function() {
            var self = this;
            self.isOpen = !self.isOpen;
        }
    };
    
    $.fn.modal = function(options) {
        var lazyModal = Object.create(LazyModal);
        lazyModal.init(options, this);
        
        $.data(this, 'lazyModal', lazyModal);
        
        return lazyModal;
    }
    
    $.fn.modal.options = {
        target: undefined,
        animation: 'fade',
        duration: 400,
        show: false,
        disableBackground: true,
        closeOnOutsideClick: true,
        onOpen: function() { },
        onOpening: function() { },
        onClose: function() { },
        onClosing: function() { }
    }
})(jQuery, window, document);
