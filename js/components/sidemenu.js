(function($, window, document, undefined) {
    
    var LazySideMenu =  {
        init: function(options, elem) {
            var self = this;
            self.elem = elem;
            self.$elem = $(elem);
            self.isOpen = false;
            self.width = self.$elem.width();
            self.isPercentageWidth = self.$elem.hasClass('full');
            self.options = $.extend({}, $.fn.sidemenu.options, options);

            if (self.options.target !== undefined) {
                self.addEvents();
            }
            
            self.setStartPosition();
        },
        addEvents: function() {
            var self = this;
            $(self.options.target).on('click', function() {
                self.openMenu();
            });
            
            if (self.$elem.find('.close').length) {
                $(self.$elem.find('.close')).on('click', function() {
                    self.openMenu();
                });
            } 
        },
        setStartPosition: function() {
            var self = this;
            switch (self.options.position.toLowerCase()) {
                case 'right':
                    self.$elem.addClass('right');
                    break;
            }
        },
        getSlideToPosition: function() {
            var pos, self = this;
            var opt = self.options.position.toLowerCase();
            
            if (opt == 'left' || opt == 'right') {
                if (self.isPercentageWidth) {
                    pos = (self.isOpen ? -100 : 0) + '%';
                } else {
                    pos = self.width * (self.isOpen ? -1 : 0) + 'px';
                }
                
                return opt == 'left' ? { left: pos } : { right: pos };
            }
        },
        openMenu: function() {
            var self = this;
            var position = self.getSlideToPosition();
            
            self.$elem.animate(position, {
                duration: self.options.duration,
                complete: function() {
                    self.isOpen = !self.isOpen;
                }
            });
        }
    };
    
    $.fn.sidemenu = function(options) {
        var lazySideMenu = Object.create(LazySideMenu);
        lazySideMenu.init(options, this);
        
        $.data(this, 'lazySideMenu', lazySideMenu);
        
        return lazySideMenu;
    }
    
    $.fn.sidemenu.options = {
        target: undefined,
        duration: 400,
        show: false,
        position: 'left',
        onOpen: function() { },
        onOpening: function() { },
        onClose: function() { },
        onClosing: function() { }
    }
})(jQuery, window, document);