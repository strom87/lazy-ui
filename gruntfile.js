module.exports = function(grunt) {
    
    var conf = {
        banner: '/* <%= pkg.name %> version: <%= pkg.version %> build: <%= grunt.template.today("yyyy-mm-dd") %> */\n\n',
        files: [
            'js/components/core.js',
            'js/components/modal.js',
            'js/components/sidemenu.js',
        ]
    };
    
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		uglify: {
            options: {
                banner: conf.banner,
                compress: {
                    drop_console: true,
                    global_defs: {
                        'DEBUG': false
                    },
                    dead_code: true
                }
            },
            my_target: {
                files: {
                    'js/lazy-ui.min.js': conf.files
                }
            }
        },
        concat: {
            options: {
                banner: conf.banner
            },
            dist: {
                src: conf.files,
                dest: 'js/lazy-ui.js'
            }
        },
        compass: {
			dist: {
				confg: 'config.rb'
			}
		},
        watch: {
            options: {
                spawn: false,
                interrupt: true
            },
            js: {
                files: ['js/components/*.js'],
                tasks: ['uglify', 'concat:dist']
            },
            css: {
                files: ['sass/**/*'],
                tasks: ['compass']
            }
        }
	});

    grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-compass');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-watch');

	grunt.registerTask('default', ['compass', 'uglify', 'concat:dist']);

};