require 'compass/import-once/activate'

# Set this to the root of your project when deployed:
project_type = :stand_alone
http_path = "/"
css_dir = "css"
sass_dir = "sass"
fonts_dir = "fonts"
images_dir = "img"
javascripts_dir = "js"
output_style = :expanded
relative_assets = true
line_comments = false
preferred_syntax = :sass